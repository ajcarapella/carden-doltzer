<?php get_template_part('includes/header'); ?>

<!-- Home Slider -->

<div id="homeCarouselStatic" class="carousel">
    <div class="carousel-inner">
        <div class="item active">
            <img src="<?php the_field('banner_image');?>" alt="<?php the_title();?>"/>
        </div>
    </div>    
</div>


<div class="container main-contain">
  <div class="row">

    <div class="col-xs-12 col-sm-8">
        <?php get_template_part('includes/loops/content', 'page'); ?>
    </div>
    
    <div class="col-xs-12 col-sm-4 no-pad" id="sidebar" role="navigation">
      <img class="sidebar-img w-100" src="<?php echo home_url('/'); ?>wp-content/themes/carden/images/S_TrustExp.jpg" alt="Trust Experience"/><br><br>
      <img class="sidebar-img w-100 add-is" src="<?php echo home_url('/'); ?>wp-content/themes/carden/images/S_HappyClients.jpg" alt="Trust Experience"/>
    </div>
    
  </div><!-- /.row -->
</div><!-- /.container -->

<?php get_template_part('includes/footer'); ?>
