

<footer class="site-footer">
	
  <div class="row">
	  
	  
    <div class="col-lg-12 site-sub-footer">
      <p>&copy; <?php get_copyright(); ?> <a href="<?= home_url('/') ?>">Carden Dotzler, PLLC</a><br>Powered by <a href="http://total-advertising.com/" title="Total Advertising | Full-Service Advertising Agency">Total Advertising</a></p>
    </div>
  </div>
  

  
</footer>
<div class="container">
  <div class="row">
	  <div class="col-xs-12 col-sm-4 col-sm-push-8">
	    <div class="estimate-form">
  		  <div id="title-bar">
          FREE Consultation
          <span class="close">&times;</span>
        </div>
  		  <em>Schedule now - No obligation required</em>
  	    <?php echo do_shortcode('[contact-form-7 id="59" title="Contact form"]');?>
  		</div>
	  </div>
  </div>
</div>

			

<?php wp_footer(); ?>
</body>
</html>
