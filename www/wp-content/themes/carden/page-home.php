<?php /*
Template Name: Home Page
*/ 
get_template_part('includes/header'); ?>

<!-- Home Slider -->

<div id="homeCarouselStatic" class="carousel hcs">
    <div class="carousel-inner">
        <div class="item active">
            <img class="hidden-xs" src="<?php echo home_url('/'); ?>wp-content/themes/carden/images/D_home1.jpg" alt="Slider 1"/>
            <img class="hidden-sm hidden-md hidden-lg" src="<?php echo home_url('/'); ?>wp-content/themes/carden/images/M_home.jpg" alt="Slider 1"/>
        </div>
    </div>    
</div>

<!-- End Carousel -->

<div id="homeCarousel" class="carousel slide hidden-xs">
    <div class="carousel-inner">
        <div class="item active">
            <img src="<?php echo home_url('/'); ?>wp-content/themes/carden/images/Slider1.jpg" alt="Slider 1"/>
        </div>
        <div class="item">
            <img src="<?php echo home_url('/'); ?>wp-content/themes/carden/images/Slider2.jpg" alt="Slider 2"/>
        </div>
        <div class="item">
            <img src="<?php echo home_url('/'); ?>wp-content/themes/carden/images/Slider3.jpg" alt="Slider 3"/>
        </div>
        
	    <ol class="carousel-indicators">
	        <li data-target="#homeCarousel" data-slide-to="0" class="active" contenteditable="false"></li>
	        <li data-target="#homeCarousel" data-slide-to="1" class="" contenteditable="false"></li>
	        <li data-target="#homeCarousel" data-slide-to="2" class="" contenteditable="false"></li>
	    </ol>
	           
    </div>    

</div>




<div id="homeCarouselMobile" class="carousel slide hidden-sm hidden-md hidden-lg">
    <div class="carousel-inner">
        <div class="item active">
            <img src="<?php echo home_url('/'); ?>wp-content/themes/carden/images/M_home1.jpg" alt="Slider 1"/>
            <div class="carousel-caption">
            	<h2>It's our extensive background and<br>experience that sets us apart</h2>
            </div>
        </div>
        <div class="item">
            <img src="<?php echo home_url('/'); ?>wp-content/themes/carden/images/M_home2.jpg" alt="Slider 2"/>
            <div class="carousel-caption">
            	<h2>Four attorneys<br>One experienced legal team</h2>
            </div>
        </div>
        <div class="item">
            <img src="<?php echo home_url('/'); ?>wp-content/themes/carden/images/M_home3.jpg" alt="Slider 3"/>
            <div class="carousel-caption">
            	<h2>Our client testimonials<br>speak for themselves.</h2>
            </div>
        </div>
        <div class="item">
            <img src="<?php echo home_url('/'); ?>wp-content/themes/carden/images/M_home4.jpg" alt="Slider 4"/>
            <div class="carousel-caption">
            	<h2>Our client testimonials<br>speak for themselves.</h2>
            </div>
        </div>
        <div class="item">
            <img src="<?php echo home_url('/'); ?>wp-content/themes/carden/images/M_home5.jpg" alt="Slider 5"/>
            <div class="carousel-caption">
            	<h2>Our client testimonials<br>speak for themselves.</h2>
            </div>
        </div>
        
	    <ol class="carousel-indicators">
	        <li data-target="#homeCarouselMobile" data-slide-to="0" class="active" contenteditable="false"></li>
	        <li data-target="#homeCarouselMobile" data-slide-to="1" class="" contenteditable="false"></li>
	        <li data-target="#homeCarouselMobile" data-slide-to="2" class="" contenteditable="false"></li>
	        <li data-target="#homeCarouselMobile" data-slide-to="3" class="" contenteditable="false"></li>
	        <li data-target="#homeCarouselMobile" data-slide-to="4" class="" contenteditable="false"></li>
	    </ol>
	           
    </div>    

</div>





<div class="container main-contain">
  <div class="row">

    <div class="col-xs-12 col-sm-8">
	    <h1>Trusted Lawyers in Syracuse, NY</h1>
	    <p>Consulting with a lawyer about your situation in Syracuse, NY, may help you determine how to best proceed with your matter. Whether you need general advice, particular answers, or assistance with a pending court case, the office of Carden Dotzler is at your service.</p>
	    <p>We take pride in the personal attention that we bring to our clients and their cases. We can care for every detail of your case while you focus on other things. Our attorney can design a strategy and file pleadings and motions that we’ll use to support your legal position. When you have to appear in court, you’ll have our lawyer at your side. We routinely assist those who are dealing with:</p>
	    <p>• Family law matters<br>
• Traffic citations<br>
• Criminal offenses<br>
• White collar crimes<br>
	    </p>
	    <p>When you need a lawyer working hard to protect your rights in Syracuse or the surrounding areas, the office of Carden Dotzler is just a call away. Contact us to request your initial consultation.</p>
	    

    </div>
    
    <div class="col-xs-12 col-sm-4 no-pad" id="sidebar" role="navigation">
      <img class="sidebar-img w-100" src="<?php echo home_url('/'); ?>wp-content/themes/carden/images/S_TrustExp.jpg" alt="Trust Experience"/><br><br>
      <img class="sidebar-img w-100 add-is" src="<?php echo home_url('/'); ?>wp-content/themes/carden/images/S_HappyClients.jpg" alt="Trust Experience"/><br><br><br>
    </div>
    
  </div><!-- /.row -->
</div><!-- /.container -->

<?php get_template_part('includes/footer'); ?>
