(function ($) {
	
	"use strict";

	$(document).ready(function() {

		// Comments
		$(".commentlist li").addClass("panel panel-default");
		$(".comment-reply-link").addClass("btn btn-default");
	
		// Forms
		$('select, input[type=text], input[type=email], input[type=password], textarea').addClass('form-control');
		$('input[type=submit]').addClass('btn btn-primary');
		
		//Add Overlay To Page When Navigation is Opened
		$('.navbar-toggle').click(function(){
			$('.overlay-nav').toggleClass('show-hide');
		});
		
		//Sticky Icky
		var stickyNavTop = $('.bottom-nav-container').offset().top;
		var stickyNav = function(){
		var scrollTop = $(window).scrollTop();
		      
		if (scrollTop > stickyNavTop) { 
		    $('.bottom-nav-container').addClass('sticky');
		} else {
		    $('.bottom-nav-container').removeClass('sticky'); 
		}
		};
		 
		stickyNav();
		 
		$(window).scroll(function() {
		  stickyNav();
		});

		// Estimate Form
		var $form = $('.estimate-form');

		$('.wpcf7-email').focus(function(){
		  	$form.addClass('expand-form');
		});

		$('.estimate-form .close').click(function(){
				$form.removeClass('expand-form');
		})

	  // Carousel
    $('#homeCarousel').carousel({
        interval: 4000
    });
    
    $('#homeCarouselMobile').carousel({
        interval: 4000
    });

    // About Us Bios
    var bio = '.about-us-bio';

    $(bio).append('<button class="about-us-read-more">Read More</button>');

    var $width = $(window).width();
    if ($width < 768) {
    	$(bio+' .about-us-body').hide();
    }

    $(bio+' .about-us-read-more').click(function(){
    	var $body = $(this).siblings('.about-us-body');
    	if ($body.hasClass('expanded')) {
    		$(this).text('Read More');
    		$body.hide();
    		$body.removeClass('expanded');
    	} else {
    		$(this).text('Read Less');
        // $body.show();
    		$body.slideDown(600);
    		$body.addClass('expanded');
    	}
    });

	
		setTimeout(function(){
					   	$('.hcs .carousel-inner>.item>img').addClass('cms-banner-closed');
		},2500);
		

	});

	// Window Resize
	$(window).resize(function() {
		var $width = $(window).width();
		var $body = $('.about-us-body');
		var $read = $('.about-us-read-more');
		if ($width >= 768) {
			$body.show();
			$body.removeClass('expanded');
			$read.text('Read More');
		} else {
			$body.hide();
			$body.removeClass('expanded');
			$read.text('Read More');
		}
	});

}(jQuery));
