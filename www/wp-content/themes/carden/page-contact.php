<?php /*
Template Name: Contact
*/ 
get_template_part('includes/header'); ?>

<div id="homeCarouselStatic" class="carousel">
    <div class="carousel-inner">
        <div class="item active">
            <img src="<?php the_field('banner_image');?>" alt="<?php the_title();?>"/>
        </div>
    </div>    
</div>


<div class="container main-contain">
  <div class="row">

    <div class="col-xs-12 col-sm-8">
      <?php get_template_part('includes/loops/content', 'page'); ?>
      
      <div class="row no-pad">

        <div class="col-xs-12 contact-box-container no-pad">

  			  <div class="contact-box col-xs-12 col-md-6">
  				  	<h2>Carden &amp; Dotzler, PLLC</h2>
  				  	<h3>100 Madison Street,</h3>
  				  	<h3>AXA Tower 1, 12th Floor</h3>
  				  	<h3>Syracuse, NY 13202</h3><br>
  				  	<h3>p: 315.930.4077</h3>
  				  	<h3>f: 315.930.4075</h3>
  				  	<h3>e: administration@cardendotzler.com</h3><br>
  				  	<h3>Hours: Mon.-Fri. 9am-5pm</h3>
  			  </div>
  
  	  		<div class="col-xs-12 col-md-6 no-pad">
  	  			<!-- <iframe id="google-map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2915.584310061956!2d-76.11881208452175!3d43.05017697914656!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!3m2!1sen!2sus!4v1481395172481" width="300" height="450" frameborder="0" style="border:0" allowfullscreen></iframe> -->
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2915.824640003015!2d-76.15203804867419!3d43.04512097904461!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89d9f3be893969a1%3A0xad137e8a7d15160!2sCarden+Dotzler%2C+PLLC.!5e0!3m2!1sen!2sus!4v1493653252190" width="100%" height="233" frameborder="0" style="border:0" allowfullscreen></iframe>
  	  		</div>

        </div>
      </div>

    </div>
    
    <div class="col-xs-12 col-sm-4 no-pad" id="sidebar" role="navigation">
      <img class="sidebar-img w-100" src="<?php echo home_url('/'); ?>wp-content/themes/carden/images/S_TrustExp.jpg" alt="Trust Experience"/><br><br>
      <img class="sidebar-img w-100 add-is" src="<?php echo home_url('/'); ?>wp-content/themes/carden/images/S_HappyClients.jpg" alt="Trust Experience"/>
    </div>
    
  </div><!-- /.row -->
</div><!-- /.container -->

<?php get_template_part('includes/footer'); ?>

